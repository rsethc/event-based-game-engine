#pragma once
#include "engine.h"

/*  If your code already refers to your DLL, it will be
	implicitly loaded and you do not have to use a
	RequiredService to require it to exist. However if
	it is implicitly loaded and you do use this feature,
	the operating system is supposed to automatically avoid
	loading something that was already loaded from the same
	file path redundantly.  */
struct RequiredService
{
    void* code_ptr;
    RequiredService (std::string dll_name);
    ~RequiredService ();
};
