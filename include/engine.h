#pragma once
#include "event.h"
#include "input.h"
#include <SDL2/SDL.h>
#include <string>

struct EngineError : std::exception
{
    std::string reason;
    EngineError (std::string reason);
    const char* what () const noexcept override;
};

struct Engine
{
    struct LibSDL2
    {
        LibSDL2 ();
        ~LibSDL2 ();
    }
    LibSDL2Instance;

    struct Window
    {
        SDL_Window* GameWindow;
        Window ();
        ~Window ();
    }
    WindowInstance;

    struct GLContext
    {
        SDL_GLContext Context;
        GLContext (SDL_Window* window);
        ~GLContext ();
    }
    GLContextInstance;

    struct LoadGL
    {
        LoadGL ();
    }
    LoadedGL;

    struct GameCode
    {
        void* code_ptr;
        GameCode ();
        ~GameCode ();
    };

    Event GameStart;
    Event GameFrame;
    Event GameClosing;

    InputService InputServiceInstance {&GameFrame};

    Engine ();
};
extern Engine* EngineInstance; /* NULL at program load to avoid accidental
                                  RAII initialization ordering bugs. */
