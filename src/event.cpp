#include "event.h"

void Event::Trigger ()
{
    for (Subscriber* sub : subscribers) sub->Execute();
}

Subscriber::Subscriber (Event* subscribe_to)
{
    event = subscribe_to;
    pos_in_vec = event->subscribers.size();
    event->subscribers.push_back(this);
}
Subscriber::~Subscriber ()
{
    Subscriber* moving = event->subscribers.back();
    event->subscribers[pos_in_vec] = moving;
    moving->pos_in_vec = pos_in_vec;
    event->subscribers.pop_back();
}

Subscriber_Func::Subscriber_Func (Event* subscribe_to, void (*func) (void* userdata), void* userdata)
    : func(func), userdata(userdata), Subscriber(subscribe_to)
{}
void Subscriber_Func::Execute ()
{
    func(userdata);
}
